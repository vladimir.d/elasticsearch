1. Make sure the minimal memory available for the application is 1Gb

2. Security

https://www.elastic.co/guide/en/elasticsearch/reference/7.13/security-minimal-setup.html#security-create-builtin-users

In terminal window, set the passwords for the built-in users by running the `elasticsearch-setup-passwords` utility.

Using the auto parameter outputs randomly-generated passwords to the console that you can change later if necessary:

`./bin/elasticsearch-setup-passwords auto`

If you want to use your own passwords, run the command with the interactive parameter instead of the auto parameter. Using this mode steps you through password configuration for all of the built-in users.

`./bin/elasticsearch-setup-passwords interactive`

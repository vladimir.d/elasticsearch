FROM docker.elastic.co/elasticsearch/elasticsearch:7.13.2

RUN yum -y install \
					sudo \
					vim-enhanced \
					telnet \
					lsof \
					net-tools \
					nmap \
					nginx

# install supervisor
RUN dnf update -y && \
	dnf install epel-release -y && \
	yum update -y && \
	yum -y install supervisor

# Elastic Search
RUN bin/elasticsearch-plugin install --batch ingest-attachment

RUN mkdir -p /app/data/elasticsearch
ADD config/elasticsearch /app/data/elasticsearch/config

RUN mkdir -p /app/data/elasticsearch/data && \
	rm -rf /usr/share/elasticsearch/data && \
	ln -s /app/data/elasticsearch/data /usr/share/elasticsearch/data && \
	rm -rf /usr/share/elasticsearch/config && \
	ln -s /app/data/elasticsearch/config /usr/share/elasticsearch/config && \
	mkdir -p /app/data/elasticsearch/logs && \
	rm -rf /usr/share/elasticsearch/logs && \
	ln -s  /app/data/elasticsearch/logs /usr/share/elasticsearch/logs && \
	chown -R elasticsearch /app/data/elasticsearch/ /usr/share/elasticsearch/data /usr/share/elasticsearch/config /usr/share/elasticsearch/logs

# nginx
ADD config/nginx /app/data/nginx
RUN rm -rf /etc/nginx/nginx.conf && \
    ln -s /app/data/nginx/conf/nginx.conf /etc/nginx/nginx.conf && \
	rm -rf /usr/share/nginx/html && \
	ln -sf /app/data/nginx/www /usr/share/nginx/html && \
    rm -rf /var/lib/nginx/ && \
    mkdir -p /run/nginx/tmp && \
    ln -sf /run/nginx /var/lib/nginx && \
    ln -sf /run/nginx-error.log /var/log/nginx/error.log && \
    ln -sf /run/nginx-access.log /var/log/nginx/access.log

EXPOSE 9200 9300 80

# supervisor
# ADD config/supervisor/ /etc/supervisor/conf.d/
ADD config/supervisor/ /app/data/supervisor/
RUN rm -rf /etc/supervisord.conf && \
	mkdir -p /etc/supervisor && \
    ln -sf /app/data/supervisor/supervisord.conf /etc/supervisor/supervisord.conf && \
    ln -sf /app/data/supervisor/conf.d /etc/supervisor/conf.d && \
    ln -sf /run/supervisord.log /var/log/supervisor/supervisord.log

COPY start.sh /app/code/

CMD [ "/app/code/start.sh" ]
